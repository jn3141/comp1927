// GameView.c ... GameView ADT implementation

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "Globals.h"
#include "Game.h"
#include "GameView.h"
#include "Map.h" //... if you decide to use the Map ADT

typedef struct _player {
    int hp;                         // HP of the the player
    int turns;                      // no. turns player has taken
    LocationID trail[TRAIL_SIZE];   // player's trail
    LocationID location;            // contains the ID of their location
} player;

struct gameView {
    int turnNum;                    // number of turns
    int score;                      // current game score out of 366
    Round roundNum;                 // number of rounds
    PlayerID currPlayer;            // ID of current player
    player players[NUM_PLAYERS];    // array of player data structs
};


// Creates a new GameView to summarise the current state of the game
GameView newGameView(char *pastPlays, PlayerMessage messages[]) {
    // allocate and create the game view
    GameView gameView = malloc(sizeof(struct gameView));

    // count the number of turns and rounds
    // turns...

    if (pastPlays[0] == 'G') {
        int i;
        for (i = 0; pastPlays[i+7] == ' '; i += 8) {
            gameView->turnNum++;
        }
        gameView->turnNum++;
    }

    // rounds...
    if (gameView->turnNum >= 5) {
        gameView->round = (gameView->turnNum/5);
    } else {
        gameView->round = 0;
    }

    // set hunter stats
    for (h = 0; h < PLAYER_DRACULA; h++) {
        gameView->players[h].hp = GAME_START_HUNTER_LIFE_POINTS;
        gameView->players[h].turns = 0;
        gameView->players[h].location = UNKNOWN_LOCATION;
    }

    return gameView;
}


// Frees all memory previously allocated for the GameView toBeDeleted
void disposeGameView(GameView toBeDeleted) {
    assert(toBeDeleted != NULL);
    free(toBeDeleted);
    return;
}


//// Functions to return simple information about the current state of the game

// Get the current round
Round getRound(GameView currentView) {
    assert(currentView != NULL);
    return currentView->roundNum;
}

// Get the id of current player - ie whose turn is it?
PlayerID getCurrentPlayer(GameView currentView) {
    assert(currentView != NULL);
    assert(currentView->currPlayer >= PLAYER_LORD_GODALMING &&
           currentView->currPlayer <= PLAYER_DRACULA);
    return currentView->currPlayer;
}

// Get the current score
int getScore(GameView currentView) {
    assert(currentView != NULL);
    assert(currentView->score <= 366);
    int gameScore;
    if (currentView->score <= 0) {
        gameScore = 0;
    } else {
        gameScore = currentView->score;
    }
    return gameScore;
}

// Get the current health points for a given player
int getHealth(GameView currentView, PlayerID player) {
    assert(currentView != NULL);
    if (player == PLAYER_DRACULA) {
        assert(currentView->players[player].hp > 0);
    } else {
        assert(currentView->players[player].hp >= 0 &&
               currentView->players[player].hp <= 9)
    }
    return currentView->players[player].hp;
}

// Get the current location id of a given player
LocationID getLocation(GameView currentView, PlayerID player) {
    assert(currentView != NULL);
    if (currentView->players[player].location == UNKNOWN_LOCATION) {
        assert(getRound(currentView) == 0);
    } else if (player >= PLAYER_LORD_GODALMING &&
               player <= PLAYER_MINA_HARKER) {
        assert(currentView->players[player].location >= 0 &&
               currentView->players[player].location <= 70);
    } else {
        assert(currentView->players[player].location == CITY_UNKNOWN  ||
               currentView->players[player].location == SEA_UNKNOWN   ||
               currentView->players[player].location == HIDE          ||
               currentView->players[player].location == DOUBLE_BACK_N ||
               currentView->players[player].location == TELEPORT      ||
               currentView->players[player].location == LOCATION_UNKNOWN);
    }
    return currentView->players[player].location;
}

//// Functions that return information about the history of the game

// Fills the trail array with the location ids of the last 6 turns
void getHistory(GameView currentView, PlayerID player,
                LocationID trail[TRAIL_SIZE]) {
    assert(currentView != NULL);
    int turn;
    for (turn = 0; turn < TRAIL_SIZE; turn++) {
        trail[turn] = currentView->players[player].trail[turn];
        turn++;
    }
    return;
}

//// Functions that query the map to find information about connectivity

// Returns an array of LocationIDs for all directly connected locations

LocationID *connectedLocations(GameView currentView, int *numLocations,
                               LocationID from, PlayerID player,
                               Round round, int road, int rail,
                               int sea) {
   // Make sure the passed in data isn't BS
   assert(currentView != NULL);
   assert(start >= MIN_MAP_LOCATION && start <= MAX_MAP_LOCATION);
   assert(end >= MIN_MAP_LOCATION && end <= MAX_MAP_LOCATION);
   // Initial setup of the values to be counted
   int num = 0;
   int arrayC = 0;
   VList currPrim = g->connections[start];

   while (currPrim != NULL) { // Iterate through the 'start' list
       if (currPrim->v == end) { // Found 'end'? End here
           type[arrayC] = currPrim->type;
           num++;
           arrayC++;
           break;
       } else if (isSea(currPrim->v) == TRUE && // 'Start' and 'end'
                   isLand(start) == TRUE &&     // both 'land', and
                   isLand(end) == TRUE) {       // found a 'sea'
           VList currSec = g->connections[currPrim->v];
           while (currSec != NULL) { // Iterate through 'sea' list
               if (currSec->v == end) { // Found 'end'? End here
                   type[arrayC] = currSec->type;
                   num++;
                   arrayC++;
                   break;
               }
               currSec = currSec->next;
           }
       }
       currPrim = currPrim->next;
   }
   return num;
}
