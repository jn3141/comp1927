#!/bin/sh

./conn London Manchester
./conn London "English Channel"
./conn "Le Havre" "English Channel"
./conn Leipzig Hamburg
./conn Paris Marseilles
./conn Santander Nantes
./conn Rome Naples
./conn "Black Sea" "Ionian Sea"
./conn Munich Berlin
./conn London "Castle Dracula"
./conn Venice Florence
./conn "Atlantic Ocean" "Black Sea"
./conn Barcelona Alicante
