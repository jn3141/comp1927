#!/bin/sh
# runs each test 20 times, and times the whole thing
# to get an average time for the sort, divide the time by 20
# replace "list" with a path to the desired list file
time for t in {1..20}
do
	sort -n < list
done
