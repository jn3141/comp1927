Input	Initial	Has	Number	AvgTime	AvgTime
Size	Order	Dups	of runs	forusel	forsort
5000	random	no	20	0.0574	0.006
5000	sorted	no	20	0.0032	0.0024
5000	reverse	no	20	0.0026  0.0038
5000	random	yes	20	0.0298	0.0048
5000	sorted	yes	20	0.0028	0.0012
5000	reverse	yes	20	0.0014	0.0038
10000	random	no	20	0.1600	0.01
10000	sorted	no	20	0.0072	0.0062
10000	reverse	no	20	0.0062	0.0070
10000	random	yes	20	0.2162	0.0094
10000	sorted	yes	20	0.0146	0.0024
10000	reverse	yes	20	0.0080	0.0082
20000	random	no	20	0.91285	0.0278
20000	sorted	no	20	0.0184	0.0142
20000	reverse	no	20	0.0170	0.0182
20000	random	yes	20	0.83745	0.0268
20000	sorted	yes	20	0.0828	0.0162
20000	reverse	yes	20	0.0160	0.0164
