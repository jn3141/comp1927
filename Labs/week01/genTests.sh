#!/bin/sh
# replace "10000" with the desired number of items for the list
echo === Making Tests ===
seq 10000 > sorted10000
gsort -nr sorted10000 > revSorted10000
gsort -R sorted10000 > random10000
