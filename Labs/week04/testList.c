// testList.c - testing DLList data type
// Written by John Shepherd, March 2013
// Expanded by Justin Ng

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "DLList.h"

void basicTest(void);
void emptyTest(void);
void oneTest(void);
void twoTest(void);
void threeTest(void);

int main(int argc, char *argv[]) {

    basicTest();
    emptyTest();
    oneTest();
    twoTest();
    threeTest();
    printf("ALL TEST PASSED! YOU ARE (kind of) AWESOME!\n");

	return EXIT_SUCCESS;
}

void basicTest(void) {
    printf("---TEST -1 : BASIC TEST---\n");
    DLList stdList;
    FILE * std = fopen("text","r");
	stdList = getDLList(std);
    printf("List without any modification:\n");
	putDLList(stdout,stdList);
    printf("\n");
	assert(validDLList(stdList));

    freeDLList(stdList);
    printf("\n");
    return;
}

void emptyTest(void) {
    printf("---TEST 0 : EMPTY TEST---\n");
    DLList emptyList;
    FILE * empty = fopen("empty","r");
    emptyList = getDLList(empty);

    // DLListAfter
    printf("List before DLListAfter():\n");
    putDLList(stdout,emptyList);
    DLListAfter(emptyList, "a");
    printf("List after DLListAfter():\n");
    putDLList(stdout,emptyList);
    printf("\n");
    assert(validDLList(emptyList));

    // DLListBefore
    printf("List before DLListBefore():\n");
    putDLList(stdout,emptyList);
    DLListBefore(emptyList, "a");
    printf("List after DLListBefore():\n");
    putDLList(stdout,emptyList);
    printf("\n");
    assert(validDLList(emptyList));

    // DLListDelete
    printf("List before DLListDelete():\n");
    putDLList(stdout,emptyList);
    DLListDelete(emptyList);
    printf("List after DLListDelete():\n");
    putDLList(stdout,emptyList);
    printf("\n");
    assert(validDLList(emptyList));

    freeDLList(emptyList);
    printf("\n");
    return;
}

void oneTest(void) {
    printf("---TEST 1 : ONE ELEMENT TEST---\n");
    DLList oneList;
    FILE * one = fopen("one","r");
    oneList = getDLList(one);

    // DLListAfter
    printf("List before DLListAfter():\n");
    putDLList(stdout,oneList);
    DLListAfter(oneList, "a");
    printf("List after DLListAfter():\n");
    putDLList(stdout,oneList);
    printf("\n");
    assert(validDLList(oneList));
    DLListDelete(oneList);

    // DLListBefore
    printf("List before DLListBefore():\n");
    putDLList(stdout,oneList);
    DLListBefore(oneList, "a");
    printf("List after DLListBefore():\n");
    putDLList(stdout,oneList);
    printf("\n");
    assert(validDLList(oneList));
    DLListDelete(oneList);

    // DLListDelete
    printf("List before DLListDelete():\n");
    putDLList(stdout,oneList);
    DLListDelete(oneList);
    printf("List after DLListDelete():\n");
    putDLList(stdout,oneList);
    printf("\n");
    assert(validDLList(oneList));

    freeDLList(oneList);
    printf("\n");
    return;
}

void twoTest(void) {
    printf("---TEST 2 : TWO ELEMENT TEST---\n");
    DLList twoList;
    FILE * two = fopen("two","r");
    twoList = getDLList(two);

    printf("---PART A : ADJUSTING ELEMENT ONE---\n");
    // DLListAfter
    DLListMoveTo(twoList, 1);
    printf("List before DLListAfter():\n");
    putDLList(stdout,twoList);
    DLListAfter(twoList, "a");
    printf("List after DLListAfter():\n");
    putDLList(stdout,twoList);
    printf("\n");
    assert(validDLList(twoList));
    DLListDelete(twoList);

    // DLListBefore
    DLListMoveTo(twoList, 1);
    printf("List before DLListBefore():\n");
    putDLList(stdout,twoList);
    DLListBefore(twoList, "a");
    printf("List after DLListBefore():\n");
    putDLList(stdout,twoList);
    printf("\n");
    assert(validDLList(twoList));
    DLListDelete(twoList);

    // DLListDelete
    DLListMoveTo(twoList, 1);
    printf("List before DLListDelete():\n");
    putDLList(stdout,twoList);
    DLListDelete(twoList);
    printf("List after DLListDelete():\n");
    putDLList(stdout,twoList);
    printf("\n");
    assert(validDLList(twoList));
    DLListBefore(twoList,"one");

    printf("---PART B : ADJUSTING ELEMENT TWO---\n");
    // DLListAfter
    DLListMoveTo(twoList, 2);
    printf("List before DLListAfter():\n");
    putDLList(stdout,twoList);
    DLListAfter(twoList, "a");
    printf("List after DLListAfter():\n");
    putDLList(stdout,twoList);
    printf("\n");
    assert(validDLList(twoList));
    DLListDelete(twoList);

    // DLListBefore
    DLListMoveTo(twoList, 2);
    printf("List before DLListBefore():\n");
    putDLList(stdout,twoList);
    DLListBefore(twoList, "a");
    printf("List after DLListBefore():\n");
    putDLList(stdout,twoList);
    printf("\n");
    assert(validDLList(twoList));
    DLListDelete(twoList);

    // DLListDelete
    DLListMoveTo(twoList, 2);
    printf("List before DLListDelete():\n");
    putDLList(stdout,twoList);
    DLListDelete(twoList);
    printf("List after DLListDelete():\n");
    putDLList(stdout,twoList);
    printf("\n");
    assert(validDLList(twoList));
    DLListBefore(twoList,"two");

    freeDLList(twoList);
    printf("\n");
    return;
}

void threeTest(void) {
    printf("---TEST 3 : THREE ELEMENT TEST---\n");
    DLList threeList;
    FILE * three = fopen("three","r");
    threeList = getDLList(three);

    printf("---PART A : ADJUSTING ELEMENT ONE---\n");
    // DLListAfter
    DLListMoveTo(threeList, 1);
    printf("List before DLListAfter():\n");
    putDLList(stdout,threeList);
    DLListAfter(threeList, "a");
    printf("List after DLListAfter():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListDelete(threeList);

    // DLListBefore
    DLListMoveTo(threeList, 1);
    printf("List before DLListBefore():\n");
    putDLList(stdout,threeList);
    DLListBefore(threeList, "a");
    printf("List after DLListBefore():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListDelete(threeList);

    // DLListDelete
    DLListMoveTo(threeList, 1);
    printf("List before DLListDelete():\n");
    putDLList(stdout,threeList);
    DLListDelete(threeList);
    printf("List after DLListDelete():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListBefore(threeList,"one");

    printf("---PART B : ADJUSTING ELEMENT TWO---\n");
    // DLListAfter
    DLListMoveTo(threeList, 2);
    printf("List before DLListAfter():\n");
    putDLList(stdout,threeList);
    DLListAfter(threeList, "a");
    printf("List after DLListAfter():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListDelete(threeList);

    // DLListBefore
    DLListMoveTo(threeList, 2);
    printf("List before DLListBefore():\n");
    putDLList(stdout,threeList);
    DLListBefore(threeList, "a");
    printf("List after DLListBefore():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListDelete(threeList);

    // DLListDelete
    DLListMoveTo(threeList, 2);
    printf("List before DLListDelete():\n");
    putDLList(stdout,threeList);
    DLListDelete(threeList);
    printf("List after DLListDelete():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListBefore(threeList,"two");

    printf("---PART C : ADJUSTING ELEMENT THREE---\n");
    // DLListAfter
    DLListMoveTo(threeList, 3);
    printf("List before DLListAfter():\n");
    putDLList(stdout,threeList);
    DLListAfter(threeList, "a");
    printf("List after DLListAfter():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListDelete(threeList);

    // DLListBefore
    DLListMoveTo(threeList, 3);
    printf("List before DLListBefore():\n");
    putDLList(stdout,threeList);
    DLListBefore(threeList, "a");
    printf("List after DLListBefore():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListDelete(threeList);

    // DLListDelete
    DLListMoveTo(threeList, 3);
    printf("List before DLListDelete():\n");
    putDLList(stdout,threeList);
    DLListDelete(threeList);
    printf("List after DLListDelete():\n");
    putDLList(stdout,threeList);
    printf("\n");
    assert(validDLList(threeList));
    DLListBefore(threeList,"three");

    freeDLList(threeList);
    return;
}
