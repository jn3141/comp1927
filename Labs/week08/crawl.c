// crawl.c ... build a graph of part of the web
// Written by John Shepherd, September 2015
// Uses the cURL library and functions by Vincent Sanders <vince@kyllikki.org>
// Modified so that it follows any URLs it finds, building a graph
// of traversed URLS before hitting the MaxURLsInGraph limit.



#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <curl/curl.h>
#include "stack.h"
#include "set.h"
#include "graph.h"
#include "queue.h"
#include "html.h"
#include "url_file.h"

#define BUFSIZE 1024

void setFirstURL(char *, char *);
void normalise(char *, char *, char *, char *, int);
//static int bfs(Graph g, char *url, Set seen);

int main(int argc, char **argv)
{
	URL_FILE *handle;
	char buffer[BUFSIZE];
	char baseURL[BUFSIZE];
	char firstURL[BUFSIZE];
	char next[BUFSIZE];
	int  maxURLs;

	if (argc > 2) {
		strcpy(baseURL,argv[1]);
		setFirstURL(baseURL,firstURL);
		maxURLs = atoi(argv[2]);
		if (maxURLs < 40) maxURLs = 40;
	}
	else {
		fprintf(stderr, "Usage: %s BaseURL MaxURLs\n",argv[0]);
		exit(1);
	}

	// You need to modify the code below to implement:
	//
	// add firstURL to the ToDo list
	// initialise Graph to hold up to maxURLs
	// initialise set of Seen URLs
	// while (ToDo list not empty and Graph not filled) {
	//    grab Next URL from ToDo list
	//    if (not allowed) continue
	//    foreach line in the opened URL {
	//       foreach URL on that line {
	//          if (Graph not filled or both URLs in Graph)
	//             add an edge from Next to this URL
	//          if (this URL not Seen already) {
	//             add it to the Seen set
	//             add it to the ToDo list
	//          }
	//       }
    //    }
	//    close the opened URL
	//    sleep(1)
	// }

	Queue toDo = newQueue();
	enterQueue(toDo, firstURL); //add firstURL to the ToDo list
	Graph g = newGraph(maxURLs); //initialise Graph to hold up to maxURLS
	Set seenURLS = newSet(); //initialise set of seen URLS
	insertInto(seenURLS, firstURL);

	Set seen = newSet();

	if (!(handle = url_fopen(firstURL, "r"))) { // if you can't open as read only, program /kill
		fprintf(stderr,"Couldn't open %s\n", next);
		exit(1);
	}

	while(!emptyQueue(toDo) && nVertices(g) < maxURLs) { // while the toDo list isn't empty and the graph isn't filled
		strcpy(next,leaveQueue(toDo)); // grab next from toDo list

		if(strstr(firstURL, "unsw") == NULL) return EXIT_FAILURE;// check that the url is allowed

		printf("lola\n");

		while(!url_feof(handle)) { //  for each line in the opened url...
			url_fgets(buffer,sizeof(buffer),handle);
			//fputs(buffer,stdout);
			int pos = 0;
			char result[BUFSIZE];
			memset(result,0,BUFSIZE);

			while ((pos = GetNextURL(buffer, firstURL, result, pos)) > 0) { //for each url on that line
				printf("Found: '%s'\n",result);
				memset(result,0,BUFSIZE);

				if(nVertices(g) < maxURLs || (isElem(seen,next) == 1 && isElem(seen,result) == 1)) { // the graph isn't filled or both urls in the graph
					addEdge(g,next,result);
				}

				if(!isElem(seen,result)) { // url not in seen
					insertInto(seen,result);
					enterQueue(toDo,result);
				}

			}
		}
	}
	url_fclose(handle);
	sleep(1);
	return 0;
}

// setFirstURL(Base,First)
// - sets a "normalised" version of Base as First
// - modifies Base to a "normalised" version of itself
void setFirstURL(char *base, char *first)
{
	char *c;
	if ((c = strstr(base, "/index.html")) != NULL) {
		strcpy(first,base);
		*c = '\0';
	}
	else if (base[strlen(base)-1] == '/') {
		strcpy(first,base);
		strcat(first,"index.html");
		base[strlen(base)-1] = '\0';
	}
	else {
		strcpy(first,base);
		strcat(first,"/index.html");
	}
}

/*static int bfs(Graph g, char *url, Set seen)
{
   int i;
   Queue q = newQueue(); //Q.Q
   enterQueue(q,url); // add url to queue
   while (!emptyQueue(q)) {
      char * y = leaveQueue(q);
      char * x = leaveQueue(q);
      if (isElem(seen,x)) continue;
      for (i = 0; i < nVertices(g); i++) {
         if (!isConnected(g,x,y)) continue;
         if (!isElem(seen,y)) enterQueue(q,y);
      }
   }
   ;
}
*/
