// Graph.c ... implementation of Graph ADT
// Written by John Shepherd, May 2013

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "Graph.h"
#include "Queue.h"

// graph representation (adjacency matrix)
typedef struct GraphRep {
	int    nV;    // #vertices
	int    nE;    // #edges
	int  **edges; // matrix of weights (0 == no edge)
} GraphRep;

// check validity of Vertex
int validV(Graph g, Vertex v) {
	return (g != NULL && v >= 0 && v < g->nV);
}

// make an edge
Edge mkEdge(Graph g, Vertex v, Vertex w) {
	assert(g != NULL && validV(g,v) && validV(g,w));
	Edge new = {v,w}; // struct assignment
	return new;
}

// insert an Edge
// - sets (v,w) and (w,v)
void insertEdge(Graph g, Vertex v, Vertex w, int wt) {
	assert(g != NULL && validV(g,v) && validV(g,w));
	if (g->edges[v][w] == 0) {
		g->edges[v][w] = wt;
		g->edges[w][v] = wt;
		g->nE++;
	}
}

// remove an Edge
// - unsets (v,w) and (w,v)
void removeEdge(Graph g, Vertex v, Vertex w) {
	assert(g != NULL && validV(g,v) && validV(g,w));
	if (g->edges[v][w] != 0) {
		g->edges[v][w] = 0;
		g->edges[w][v] = 0;
		g->nE--;
	}
}

// create an empty graph
Graph newGraph(int nV) {
	assert(nV > 0);
	int v, w;
	Graph new = malloc(sizeof(GraphRep));
	assert(new != 0);
	new->nV = nV; new->nE = 0;
	new->edges = malloc(nV*sizeof(int *));
	assert(new->edges != 0);
	for (v = 0; v < nV; v++) {
		new->edges[v] = malloc(nV*sizeof(int));
		assert(new->edges[v] != 0);
		for (w = 0; w < nV; w++)
			new->edges[v][w] = 0;
	}
	return new;
}

// free memory associated with graph
void dropGraph(Graph g) {
	assert(g != NULL);
	// not needed for this lab
}

// display graph, using names for vertices
void showGraph(Graph g, char **names) {
	assert(g != NULL);
	printf("#vertices=%d, #edges=%d\n\n",g->nV,g->nE);
	int v, w;
	for (v = 0; v < g->nV; v++) {
		printf("%d %s\n",v,names[v]);
		for (w = 0; w < g->nV; w++) {
			if (g->edges[v][w]) {
				printf("\t%s (%d)\n",names[w],g->edges[v][w]);
			}
		}
		printf("\n");
	}
}


// find a path between two vertices using breadth-first traversal
// only allow edges whose weight is less than "max"
int findPath(Graph g, Vertex src, Vertex dest, int max, int *path) {
    // holds an array of whether a place has been visited
    int visited[30] = {0};

    // holds the connections between cities
    Vertex flights[30];

    // holds the reversed final flight path
    int reversed[30];

    // fill flights[] and reversed[] arrays with -1's
    int i;
    for (i = 0; i < 30; i++) {
        flights[i] = reversed[i] = -1;
    }

    // start the counter of number of hops / cities visited
    int counter = 0;

    // start up the TODO queue, add the first element, and set it as
    // visited; also set up the flag for early exit
    Queue q = newQueue();
    QueueJoin(q,src);
    visited[src] = 1;
    int isFound = 0;

    // while the queue isn't empty, and the early exit flag isn't filled
    while (!QueueIsEmpty(q) && !isFound) {
        Vertex y;
        Vertex x = QueueLeave(q);
        for (y = 0; y < g->nV; y++) {
            // is the possible connection within fuel range?
            if (g->edges[x][y] < 0 || g->edges[x][y] > max) {
                continue;
            }

            // have I modified this value before?
            if (flights[y] < 0) {
                flights[y] = x;
            }

            // is this the destination? set the early exit flag if so
            if (y == dest) {
                isFound = 1;
                break;
            }

            // haven't touched this place before? add it the queue
            if (!visited[y]) {
                QueueJoin(q,y);
                visited[y] = 1;
            }
        }
    }

    // so we found the path? time to load it in!
    if (isFound) {
        // follows the connections of the cities from dest to src,
        // loading into an array
        Vertex v;
        i = 0;
        for (v = dest; v != src; v = flights[v]) {
            reversed[i] = v;
            i++;
            counter++;
        }

        // now takes that reversed array, and loads it in the right way
        // into path
        reversed[i] = src;
        counter++;
        int start = 0;
        int end = counter - 1;
        while (end >= 0) {
            path[start] = reversed[end];
            start++;
            end--;
        }
    }
    return counter;
}
