#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef unsigned char byte;
typedef u_int32_t vsize_t;
typedef u_int32_t vlink_t;
typedef u_int32_t vaddr_t;

typedef struct free_list_header {
   u_int32_t magic;  // ought to contain MAGIC_FREE
   vsize_t size;     // # bytes in this block (including header)
   vlink_t next;     // memory[] index of next free block
   vlink_t prev;     // memory[] index of previous free block
} free_header_t;

typedef struct alloc_block_header {
   u_int32_t magic;  // ought to contain MAGIC_ALLOC
   vsize_t size;     // # bytes in this block (including header)
} alloc_header_t;

int main(int argc, char  *argv[]) {
    printf("unsigned char is %d\n",sizeof(unsigned char));
    printf("unsigned char ptr is %d\n",sizeof(unsigned char *));
    printf("unsigned char ptr is %d\n",sizeof(unsigned char *));
    printf("alloc_header_t is %d\n",sizeof(alloc_header_t));
    printf("alloc_header_t ptr is %d\n",sizeof(alloc_header_t *));
    printf("free_header_t is %d\n",sizeof(free_header_t));
    printf("free_header_t ptr is %d\n",sizeof(free_header_t *));
    return EXIT_SUCCESS;
}
