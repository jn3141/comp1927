//
//  COMP1927 Assignment 1 - Vlad: the memory allocator
//  allocator.c ... implementation
//
//  Created by Liam O'Connor on 18/07/12.
//  Modified by John Shepherd in August 2014, August 2015
//  Copyright (c) 2012-2015 UNSW. All rights reserved.
//

#include "allocator.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define FREE_HEADER_SIZE  sizeof(struct free_list_header)
#define ALLOC_HEADER_SIZE sizeof(struct alloc_block_header)
#define MAGIC_FREE     0xDEADBEEF
#define MAGIC_ALLOC    0xBEEFDEAD

#define BEST_FIT       1
#define WORST_FIT      2
#define RANDOM_FIT     3

typedef unsigned char byte;
typedef u_int32_t vsize_t;
typedef u_int32_t vlink_t;
typedef u_int32_t vaddr_t;

typedef int bool;
#define TRUE 1
#define FALSE 0

typedef struct free_list_header {
   u_int32_t magic;  // ought to contain MAGIC_FREE
   vsize_t size;     // # bytes in this block (including header)
   vlink_t next;     // memory[] index of next free block
   vlink_t prev;     // memory[] index of previous free block
} free_header_t;

typedef struct alloc_block_header {
   u_int32_t magic;  // ought to contain MAGIC_ALLOC
   vsize_t size;     // # bytes in this block (including header)
} alloc_header_t;

// Global data

static byte *memory = NULL;   // pointer to start of allocator memory
static vaddr_t free_list_ptr; // index in memory[] of first block in free list
static vsize_t memory_size;   // number of bytes malloc'd in memory[]
static u_int32_t strategy;    // allocation strategy (default = BEST_FIT)

// Private functions

static void vlad_merge();
static u_int32_t powerTwoGen(u_int32_t x);
static u_int32_t multiFourGen(u_int32_t x);
static free_header_t *addToPtr(vaddr_t index);
static vaddr_t ptrToAdd(byte *ptr);

// Input: size - number of bytes to make available to the allocator
// Output: none
// Precondition: Size >= 1024
// Postcondition: `size` bytes are now available to the allocator
//
// (If the allocator is already initialised, this function does nothing,
//  even if it was initialised with different size)

void vlad_init(u_int32_t size) {
    strategy = BEST_FIT;

    if (memory != NULL) {
        fprintf(stderr, "vlad_init: It is required "
                        "that memory == NULL\n");
        exit(EXIT_FAILURE);
    }

    if (size < 1024) {
        size = 1024;
    }

    // make size a power of 2
    size = powerTwoGen(size);

    // allocate memory and check for fails
    memory = malloc(size);
    if (memory == NULL) {
        fprintf(stderr, "vlad_init: Insufficient memory\n");
        exit(EXIT_FAILURE);
    }

    free_list_ptr = (vaddr_t) 0;
    memory_size = size;

    // set header
    free_header_t *header = addToPtr(free_list_ptr);
    header->magic = MAGIC_FREE;
    header->size = size;
    header->next = free_list_ptr;
    header->prev = free_list_ptr;

    return;
}


// Input: n - number of bytes requested
// Output: p - a pointer, or NULL
// Precondition: n < size of largest available free block
// Postcondition: If a region of size n or greater cannot be found,
//                p = NULL
//                Else, p points to a location immediately after a
//                      header block for a newly-allocated region of
//                      some size >= n + header size.

void *vlad_malloc(u_int32_t n) {

    if (memory == NULL) {
        fprintf(stderr, "vlad_malloc(1): vlad_init "
                        "must first be run\n");
        exit(EXIT_FAILURE);
    }

    if (n < 8) {
        n = 8;
    }

    int threshold = ALLOC_HEADER_SIZE + n + 2 * FREE_HEADER_SIZE;

    // make (n + ALLOC_HEADER_SIZE) a multiple of 4
    n = multiFourGen(n + ALLOC_HEADER_SIZE);

    free_header_t *block = addToPtr(free_list_ptr);
    if (block->magic != MAGIC_FREE) {
        fprintf(stderr, "vlad_malloc(2): The header "
                        "identification is corrupt\n");
        exit(EXIT_FAILURE);
    }

    // find big enough block; if one isn't available, return NULL
    while (block->size < n) {
        block = addToPtr(block->next);
        if (block->magic != MAGIC_FREE) {
            fprintf(stderr, "vlad_malloc(3): The header "
                            "identification is corrupt\n");
            printf("It was block %p\n", block);
            printf("The index was %d\n",ptrToAdd((byte*)block));
            exit(EXIT_FAILURE);
        }

        if (block == addToPtr(free_list_ptr)) {
            return NULL;
        }
    }

    // large enough block found, so assign as necessary
    alloc_header_t *newAlloc = NULL;

    if (block == addToPtr(block->next)) { // just one block
        if (block->size < threshold) { // try allocating whole = no
            return NULL;
        } else { // split the one block
            // set up the block afterwards
            // remove newly allocated block from free chain
            free_header_t *newFree =
                (free_header_t *)((byte *)block + n);
            newFree->magic = MAGIC_FREE;
            newFree->size = block->size - n;
            newFree->next = ptrToAdd((byte *)newFree);
            newFree->prev = ptrToAdd((byte *)newFree);

            free_list_ptr = ptrToAdd((byte *)newFree);

            // set up the newly allocated block
            newAlloc = (alloc_header_t *)block;
            newAlloc->magic = MAGIC_ALLOC;
            newAlloc->size = n;
        }
    } else { // more than one block
        if (block->size < threshold) { // allocate the whole block
            // remove newly allocated block from free chain
            addToPtr(block->prev)->next = block->next;
            addToPtr(block->next)->prev = block->prev;

            if (block == addToPtr(free_list_ptr)) {
                free_list_ptr = block->next;
            }

            // set up the newly allocated block
            newAlloc = (alloc_header_t *)block;
            newAlloc->magic = MAGIC_ALLOC;
            newAlloc->size = block->size;
        } else { // split the block
            // set up the block afterwards
            // remove newly allocated block from free chain
            free_header_t *newFree =
                (free_header_t *)((byte *)block + n);
            newFree->magic = MAGIC_FREE;
            newFree->size = block->size - n;
            newFree->next = block->next;
            newFree->prev = block->prev;
            addToPtr(block->prev)->next = ptrToAdd((byte *)newFree);
            addToPtr(block->next)->prev = ptrToAdd((byte *)newFree);

            if (block == addToPtr(free_list_ptr)) {
                free_list_ptr = ptrToAdd((byte *)newFree);
            }

            // set up the newly allocated block
            newAlloc = (alloc_header_t *)block;
            newAlloc->magic = MAGIC_ALLOC;
            newAlloc->size = n;
        }
    }

    return (void *)((byte *)newAlloc + ALLOC_HEADER_SIZE);
}


// Input: object, a pointer.
// Output: none
// Precondition: object points to a location immediately after a
//               header block within the allocator's memory.
// Postcondition: The region pointed to by object has been placed in the
//                free list, and merged with any adjacent free blocks;
//                the memory space can be re-allocated by vlad_malloc

void vlad_free(void *object) {

    if ((byte *)object < memory ||
        (byte *)object > (memory + memory_size)) {
        fprintf(stderr, "vlad_free: Attempt to free "
                        "via invalid pointer\n");
        fprintf(stderr, "vlad_free: The end of the memory "
                        "is %p\n", (byte*)(memory + memory_size));
        exit(EXIT_FAILURE);
    }

    free_header_t *newFree =
        (free_header_t *)((byte *)object - ALLOC_HEADER_SIZE);

    if (newFree->magic != MAGIC_ALLOC) {
        fprintf(stderr, "vlad_free: Attempt to free "
                        "non-allocated memory\n");
        fprintf(stderr, "vlad_free: the address was %p\n",
            (alloc_header_t *)((byte *)object - ALLOC_HEADER_SIZE));
        exit(EXIT_FAILURE);
    }

    free_header_t *block = addToPtr(free_list_ptr);
    if (block->magic != MAGIC_FREE) {
        fprintf(stderr, "vlad_free(1): The header "
                        "identification is corrupt\n");
        exit(EXIT_FAILURE);
    }

    while (block < newFree) {
        block = addToPtr(block->next);
        if (block->magic != MAGIC_FREE) {
            fprintf(stderr, "vlad_free(2): The header "
                            "identification is corrupt\n");
            exit(EXIT_FAILURE);
        }
        if (block == addToPtr(free_list_ptr)) {
            break;
        }
    }

    newFree->magic = MAGIC_FREE;
    newFree->size = newFree->size;
    newFree->next = ptrToAdd((byte *)block);
    newFree->prev = block->prev;

    addToPtr(block->prev)->next = ptrToAdd((byte *)newFree);
    block->prev = ptrToAdd((byte *)newFree);

    if (ptrToAdd((byte *)newFree) < free_list_ptr) {
        free_list_ptr = ptrToAdd((byte *)newFree);
    }

    vlad_merge();
}

// Input: current state of the memory[]
// Output: new state, where any adjacent blocks in the free list
//            have been combined into a single larger block; after this,
//            there should be no region in the free list whose next
//            reference is to a location just past the end of the region

static void vlad_merge() {
    free_header_t *block = addToPtr(free_list_ptr);
    if (block->magic != MAGIC_FREE) {
        fprintf(stderr, "vlad_malloc: The header "
                        "identification is corrupt\n");
        exit(EXIT_FAILURE);
    }

    bool started = FALSE;

    while (started == FALSE || block != addToPtr(free_list_ptr)) {
        if (block->magic != MAGIC_FREE) {
            fprintf(stderr, "vlad_malloc: The header "
                            "identification is corrupt\n");
            exit(EXIT_FAILURE);
        }

        while (addToPtr(block->next) ==
            (free_header_t *)((byte *)block + block->size)) {
            block->size = block->size + addToPtr(block->next)->size;
            block->next = addToPtr(block->next)->next;
            addToPtr(block->next)->prev = ptrToAdd((byte *)block);
        }

        started = TRUE;
        block = addToPtr(block->next);
    }
}

// Stop the allocator, so that it can be init'ed again:
// Precondition: allocator memory was once allocated by vlad_init()
// Postcondition: allocator is unusable until vlad_int() executed again

void vlad_end(void) {
    // ensure that memory was initiailised beforehand
    if (memory == NULL) {
        exit(EXIT_FAILURE);
    }

    free(memory);
    memory = NULL;
    free_list_ptr = -1;
    memory_size = 0;

    return;
}


// Precondition: allocator has been vlad_init()'d
// Postcondition: allocator stats displayed on stdout

void vlad_stats(void) {
    printf("free_list_ptr is %d\n", free_list_ptr);

    free_header_t * block = addToPtr(free_list_ptr);
    printf("----------------------------------\n");
    printf("The index of the free block is %d\n",
        ptrToAdd((byte*)block));
    printf("The size of the free block is %d\n",block->size);
    printf("Prev is %d\n",block->prev);
    printf("Next is %d\n",block->prev);
    printf("----------------------------------\n");

    while (addToPtr(block->next) != addToPtr(free_list_ptr)) {
        block = addToPtr(block->next);
        printf("----------------------------------\n");
        printf("The index of the free block is %d\n",
            ptrToAdd((byte*)block));
        printf("The size of the free block is %d\n",block->size);
        printf("Prev is %d\n",block->prev);
        printf("Next is %d\n",block->prev);
        printf("----------------------------------\n");
    }

    return;
}

static u_int32_t powerTwoGen(u_int32_t x) {
    while ((x != 0) && !(x & (x - 1)) == FALSE) {
        x++;
    }
    return x;
}

static u_int32_t multiFourGen(u_int32_t x) {
    while (x % 4 != 0) {
        x++;
    }
    return x;
}

static free_header_t *addToPtr(vaddr_t index) {
    return (free_header_t *)(memory + index);
}

static vaddr_t ptrToAdd(byte *ptr) {
    return (vaddr_t)(ptr - memory);
}
